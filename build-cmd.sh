#!/bin/bash

# turn on verbose debugging output for logs.
set -x
# make errors fatal
set -e
# complain about unset env variables
set -u

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

builddir="$(pwd)"
topdir=$(dirname "$0")

# load autobuild provided shell functions and variables
set +x
eval "$("$autobuild" source_environment)"
set -x

# set LL_BUILD and friends... but this script doesn't build anything
#set_build_variables convenience Release

VERSION_HEADER_FILE="${topdir}/include/gstreamer-0.10/gst/gstversion.h"
major_version=$(sed -n -E 's/#define GST_VERSION_MAJOR \(([0-9.]+)\)/\1/p' "${VERSION_HEADER_FILE}")
minor_version=$(sed -n -E 's/#define GST_VERSION_MINOR \(([0-9.]+)\)/\1/p' "${VERSION_HEADER_FILE}")
micro_version=$(sed -n -E 's/#define GST_VERSION_MICRO \(([0-9.]+)\)/\1/p' "${VERSION_HEADER_FILE}")
build=${AUTOBUILD_BUILD_ID:=0}
echo "${major_version}.${minor_version}.${micro_version}.${build}" > "${builddir}/VERSION.txt"


case "${AUTOBUILD_PLATFORM}" in 
    windows*|darwin*)
        echo Not used on $AUTOBUILD_PLATFORM
        ;;
    linux*)
        cp -r "${topdir}/include" "${builddir}"
        ;;
    *)
        fail "Unknown AUTOBUILD_PLATFORM='${AUTOBUILD_PLATFORM}'"
        ;;
esac

cp -r "${topdir}/LICENSES"  "${builddir}"


