Yes, this is lame.

No, we don't have the source tree from which this stuff was built originally, or the scripts that built it.

Yes, this is a problem.

This repo has been created in its current form to enable the creation of a package that at least
superficially conforms to the new packaging conventions.

If anyone wants to either

1. Figure out how this really should be built
2. determine that we actually don't need it or can replace it with
   something else, I'd appreciate it.
   
- _Oz Linden_
